from cv2 import *
import numpy as np
from porovnanie_histogramov import displayAllImage


def all_around_filter(image):
    kernel = np.array([[-1, -1, -1],
                       [-1, 8, -1],
                       [-1, -1, -1]])
    return filter2D(image, -1, kernel)


def all_around_filter_bigger(image):
    kernel = np.array([[0, -1, -1, -1, 0],
                       [-1, -1, -1, -1, -1],
                       [-1, -1, 20, -1, -1],
                       [-1, -1, -1, -1, -1],
                       [0, -1, -1, -1, 0]])
    return filter2D(image, -1, kernel)


def postupny_zlozeny_filter(image):
    kernel1 = np.array([[1, 0, -1],
                        [1, 0, -1],
                        [1, 0, -1]])

    kernel2 = np.array([[-1, 0, 1],
                        [-1, 0, 1],
                        [-1, 0, 1]])

    kernel3 = np.array([[1, 1, 1],
                        [0, 0, 0],
                        [-1, -1, -1]])

    kernel4 = np.array([[-1, -1, -1],
                        [0, 0, 0],
                        [1, 1, 1]])

    kernel5 = np.array([[-1, -1, 0],
                        [-1, 0, 1],
                        [0, 1, 1]])

    kernel6 = np.array([[1, 1, 0],
                        [1, 0, -1],
                        [0, -1, -1]])

    kernel7 = np.array([[-1, -1, -1],
                        [-1, 8, -1],
                        [-1, -1, -1]])

    im1 = filter2D(image, -1, kernel1)
    im2 = filter2D(image, -1, kernel2)
    im3 = filter2D(image, -1, kernel3)
    im4 = filter2D(image, -1, kernel4)
    im5 = filter2D(image, -1, kernel7)
    # im6 = filter2D(image, -1, kernel6)

    im1 = im1 - im1.min()
    im1 = im1 / im1.max()

    im2 = im2 - im2.min()
    im2 = im2 / im2.max()

    im3 = im3 - im3.min()
    im3 = im3 / im3.max()

    im4 = im4 - im4.min()
    im4 = im4 / im4.max()

    print(im1.dtype)
    print(im1.max())
    print(im1.min())

    print((im1+im2+im3+im4).max())
    print((im1+im2+im3+im4).min())

    imshow('1', im1)
    imshow('2', im2)
    imshow('3', im3)
    imshow('4', im4)
    imshow('5', im5)
    imshow('6', (im1+im3)/2)
    imshow('image', image)


filenames = [
    # '31_19-294423-180853.jpg',
    # '39_19-291464-179674.jpg',
    # '01_18-146806-90405.jpg',
    # '04_19-287201-181875.jpg',
    # '22_19-293534-180845.jpg',
    '08_19-288047-182369.jpg',
    '07_19-288047-182368.jpg',
    '06_19-288030-182357.jpg',
    '02_19-287198-181876.jpg',
    '03_19-287200-181876.jpg',
    '04_19-287201-181875.jpg',
    '26_19-293875-180701.jpg',
    '27_19-293876-180704.jpg',
    '28_19-293876-180705.jpg'
]

images = []

for f in filenames:
    images.append([f, imread('../data/' + f)])

for i in range(len(images)):
    if i == 10:
        pass

    image = images[i]
    grayscale = cvtColor(image[1], COLOR_BGR2GRAY).astype('float32') / 255.
    filtered = all_around_filter_bigger(grayscale)


    imshow(image[0], grayscale)
    imshow(image[0]+'filtered', filtered)

waitKey()

# ------------------------------------------------

# displayAllImage(images)

# kernel = np.array([[1, 1, 1],
#                    [1, -8, 1],
#                    [1, 1, 1]])
#
# kernel2 = np.array([[1, 0, -1],
#                     [1, 0, -1],
#                     [1, 0, -1]])
#
# kernel3 = np.array([[0, 0, -1],
#                     [0, 3, -1],
#                     [0, 0, -1]])
#
# kernel5 = np.array([[0, -1, -1, -1, 0],
#                     [-1, -1, -1, -1, -1],
#                     [-1, -1, 20, -1, -1],
#                     [-1, -1, -1, -1, -1],
#                     [0, -1, -1, -1, 0]])
#
# kernel6 = np.array([[-0., -0.32397198, -0.45308184, -0.32397198, -0.],
#                     [-0.32397198, -0.77345908, -1., -0.77345908, -0.32397198],
#                     [-0.45308184, -1., 11.49793948, -1., -0.45308184],
#                     [-0.32397198, -0.77345908, -1., -0.77345908, -0.32397198],
#                     [-0., -0.32397198, -0.45308184, -0.32397198, -0.]])
