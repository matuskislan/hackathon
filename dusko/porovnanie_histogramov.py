import cv2
import os
import numpy as np
from skimage.segmentation import slic  # pre superpixle
import matplotlib.pyplot as plt  # cv2 nevie (ja neviem) vykreslovat viac veci do jedneho obrazku
from datetime import datetime


def displayAllImage(images):
    for image in images:
        cv2.imshow(image[0], images[1])


def calculate3dHistogramZeroStatistics(images):
    zero_stats = np.zeros((9, len(images)))

    for i in range(len(images)):
        bins = [256, 128, 64, 32, 16, 8, 4, 2, 1]
        zero_stats[:, i] = [
            (cv2.calcHist([images[i][1]], [0, 1, 2], None, [bin] * 3, [0, 255] * 3) == 0).sum() / float(
                bin ** 3) * 100 for bin in bins]

    np.savetxt('not_zero_stats.txt', zero_stats.astype('float32').T, delimiter=',', fmt='%.2f',
               header=','.join([str(bin) for bin in bins]))


def compareWholeImageHistToSuperpixelHists():
    bin_size = 256
    path_to_files = '../train/'
    test_file = '1_31_19-294423-180853.jpg'
    num_of_segments = 200

    files = os.listdir(path_to_files)

    images = []
    test_image = None

    for f in files:
        if f != test_file:
            image = cv2.imread(path_to_files + f)
            images.append([f, image])
        else:
            test_image = cv2.imread(path_to_files + f)

    print('number of images: {}'.format(len(images)))

    normalized = test_image.astype('float32') / 255.  # neviem ako funguje cv2.normalize, ale chcem presne toto

    startTime = datetime.now()

    segments = slic(normalized, num_of_segments)

    end_time = datetime.now()
    delta_time = end_time - startTime
    print('slic duration: {} seconds'.format(delta_time.seconds + delta_time.microseconds / 1000000.))

    print('shape of segments: {}'.format(segments.shape))

    histograms = []
    real_num_of_segments = segments.max()

    startTime = datetime.now()

    for i in range(real_num_of_segments):
        mask = (segments == i).astype('uint8')
        histograms.append([
            cv2.calcHist([test_image], [0], mask, [256], [0, 255]),
            cv2.calcHist([test_image], [1], mask, [256], [0, 255]),
            cv2.calcHist([test_image], [2], mask, [256], [0, 255])
        ])

    end_time = datetime.now()
    delta_time = end_time - startTime
    delta_seconds = delta_time.seconds + delta_time.microseconds / 1000000.
    print('calchist for every segment duration: {} seconds'.format(delta_seconds))

    histograms = np.array(histograms)

    print('histograms shape: {}'.format(histograms.shape))

    average_histograms = [
        np.average(histograms[:, 0, :], 0),
        np.average(histograms[:, 1, :], 0),
        np.average(histograms[:, 2, :], 0)
    ]
    plain_histograms = [
        cv2.calcHist([test_image], [0], None, [256], [0, 255]),
        cv2.calcHist([test_image], [1], None, [256], [0, 255]),
        cv2.calcHist([test_image], [2], None, [256], [0, 255])
    ]

    colors = ['red', 'green', 'blue']

    plt.plot(plain_histograms[0] / real_num_of_segments + 50, color=colors[0], ls='solid')
    plt.plot(plain_histograms[1] / real_num_of_segments + 50, color=colors[1], ls='solid')
    plt.plot(plain_histograms[2] / real_num_of_segments + 50, color=colors[2], ls='solid')

    plt.plot(average_histograms[0], color=colors[0], ls='dashed')
    plt.plot(average_histograms[1], color=colors[1], ls='dashed')
    plt.plot(average_histograms[2], color=colors[2], ls='dashed')

    plt.show();


# ------------------------------------------------

# fig = plt.figure("Superpixels")
# plt.imshow(mark_boundaries(normalized, segments))
# plt.imshow(normalized)
# plt.show()

# ------------------------------------------------

compareWholeImageHistToSuperpixelHists()

# cv2.waitKey()
